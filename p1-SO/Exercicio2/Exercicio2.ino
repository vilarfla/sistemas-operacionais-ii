//Utilizando PushButton (botões), com um circuito PULL-UP e PULL-DOWN. Ligue 3 LEDs e 2 botões. O primeiro botão deve acender os LEDs na ordem crescente. O último botão deve apagar todos os LEDs decrescente

int led1 =8;
int led2 = 9;
int led3 = 10;
int btn_up = 2;
int btn_down =4;
int cont = 0;

void setup() {
  Serial.begin(9600);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(btn_up, INPUT);
  pinMode(btn_down, INPUT);
 
}

void loop() {
  
  int up = digitalRead(btn_up);

  int down = digitalRead(btn_down);
  
  if(up == LOW){ 
      Serial.println("up Pressionado");
      cont = cont+1;
      Serial.println(cont);
  }else{ 
    delay(1000);
  }  

   if(down == HIGH){ 
      Serial.println("down Pressionado");
      cont = cont-1;
      Serial.println(cont);
  }else{ 
    delay(1000);
  }  


  if(cont==1)
  {
    digitalWrite(led1, HIGH); 
    digitalWrite(led2, LOW);
    digitalWrite(led3, LOW);
  }else if (cont == 2){
    digitalWrite(led2, HIGH); 
    digitalWrite(led1, LOW);
    digitalWrite(led3, LOW);
  } else if (cont ==3){
    digitalWrite(led3, HIGH); 
    digitalWrite(led1, LOW);
    digitalWrite(led2, LOW);
  }
  else{
    digitalWrite(led3, LOW); 
    digitalWrite(led1, LOW);
    digitalWrite(led2, LOW);
  }
  

  delay(1000);


}
