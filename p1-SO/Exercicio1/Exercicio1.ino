//Simulando um contador binário de 3 bit. Faça os Led acenderem de acordo com a tabela abaixo.  
 
//1 = Ligado  0 = Apagado 
//Tabela:
//Led2 =4   Led1 = 2     Led0 =1
//0              0              0
//0              0              1
//0              1              0
//0              1              1
//1              0              0
//1              0              1
//1              1              0
//1              1              1

int led1 =8;
int led2 = 9;
int led3 = 10;

void setup() {
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
}

void loop() {

 digitalWrite(led1, 0);
 digitalWrite(led2, 0);
 digitalWrite(led3, 0);
 delay(1000);

 digitalWrite(led1, 0);
 digitalWrite(led2, 0);
 digitalWrite(led3, 1);
 delay(1000);

 digitalWrite(led1, 0);
 digitalWrite(led2, 1);
 digitalWrite(led3, 0);
 delay(1000);

 digitalWrite(led1, 0);
 digitalWrite(led2, 1);
 digitalWrite(led3, 1);
 delay(1000);

 digitalWrite(led1, 1);
 digitalWrite(led2, 0);
 digitalWrite(led3, 0);
 delay(1000);

 digitalWrite(led1, 1);
 digitalWrite(led2, 0);
 digitalWrite(led3, 1);
 delay(1000);

 digitalWrite(led1, 1);
 digitalWrite(led2, 1);
 digitalWrite(led3, 0);
 delay(1000);

 digitalWrite(led1, 1);
 digitalWrite(led2, 1);
 digitalWrite(led3, 1);
 delay(1000);


}
